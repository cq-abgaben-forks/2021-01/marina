nextflow.enable.dsl=2

process split_file {
  input:
    path infile
  output:
    path "${infile}.line*"
  script:
    """
    split -l 2 ${infile} -d ${infile}.line
    """
}

process count_ATG {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infile
  output:
    path "${infile}.ATGcount", emit: ATGcount
  script:
    """
    tail -n 1 ${infile} > sequenz
    grep -o -i "ATG" sequenz > grepresult
    cat grepresult | wc -l > ${infile}.ATGcount
    """
}

process count_STOP {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infile
  output:
    path "${infile}.STOPcount", emit: STOPcount
  script:
    """
    tail -n 1 ${infile} > sequenz
    egrep -o -i 'TAA|TAG|TGA' sequenz > grepresult
    cat grepresult | wc -l >> ${infile}.STOPcount
    """
}

process calculate_total_ATG {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infiles
  output:
    path "ATGcount"
  script:
    """
    cat ${infiles} | paste -sd "+" | bc > ATGcount
    """
}

process calculate_total_STOP {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infiles
  output:
    path "STOPcount"
  script:
    """
    cat ${infiles} | paste -sd "+" | bc > STOPcount
    """
}

process out {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path starts
    path stops
  output:
    path "answer"
  script:
    """
    echo -n  "Die Anzahl der Startcodons beträgt: " > answer
    cat ${starts} >> answer
    echo -n  "Die Anzahl der Stopcodons beträgt: " >> answer
    cat ${stops} >> answer
    """
}


workflow {
  if(!file(params.infile).exists()) {
    println("Input file ${params.infile} does not exist.")
    exit(1)
  }
  inchannel = channel.fromPath(params.infile)
  splitfiles = split_file(inchannel)
  startcounts = count_ATG(splitfiles.flatten())
  stopcounts = count_STOP(splitfiles.flatten())
  starts = calculate_total_ATG(startcounts.collect())
  stops = calculate_total_STOP(stopcounts.collect())
  out(starts,stops)

}



